package com.olpi;

import com.olpi.domain.Ingredient;
import com.olpi.domain.Order;
import com.olpi.domain.Pizza;
import com.olpi.exception.IlligalAmountException;

public class PalmettoPizzaApp {
    public static void main(String[] args) {

        Order order7717 = new Order(7717);
        try {
            Pizza margarita = new Pizza("Margarita", true, 2);
            margarita.addIngredient(Ingredient.GARLIC);
            margarita.addIngredient(Ingredient.CHEESE);
            order7717.addPizza(margarita);
            Pizza pepperoni = new Pizza("Pepperoni", false, 3);
            pepperoni.addIngredient(Ingredient.CHEESE);
            pepperoni.addIngredient(Ingredient.GARLIC);
            pepperoni.addIngredient(Ingredient.PEPPERONI);
            order7717.addPizza(pepperoni);
        } catch (IlligalAmountException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

        System.out.println(order7717.toString());
        System.out.println(order7717.getDate());

        Order order4372 = new Order(4372);
        try {
            Pizza baseZZ = new Pizza("BaseZZ", false, 12);
            order4372.addPizza(baseZZ);
        } catch (IlligalAmountException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

        System.out.println(order4372.toString());
        order7717.printPizzaAtribute();
    }
}
