package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.olpi.exception.IlligalAmountException;

public class Pizza {

    private String name;
    private boolean isCalzone;
    private List<Ingredient> ingredients;
    private int amount;

    public Pizza(String name, boolean isCalzone, int amount)
            throws IlligalAmountException {
        this.name = name;
        this.isCalzone = isCalzone;
        setAmount(amount);
        ingredients = new ArrayList<>(9);

        // PizzaBase and Tomato_paste is default ingredients
        if (isCalzone) {
            ingredients.add(Ingredient.PIZZA_BASE_CALZONE);
        } else {
            ingredients.add(Ingredient.PIZZA_BAZE);
        }
        ingredients.add(Ingredient.TOMATO_PASTE);
    }

    public void addIngredient(Ingredient ingred) {

        if (isIngredientExist(ingred)) {
            System.out.println(
                    "This ingredient already exists, please check the order.");
        } else if (isPizzaFull()) {
            System.out.println("You cannot add an ingredient, pizza is full!");
        } else {
            ingredients.add(ingred);
        }
    }

    public boolean isIngredientExist(Ingredient ingred) {
        return ingredients.contains(ingred);
    }

    public boolean isPizzaFull() {
        return ingredients.size() == 9;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(int amount) throws IlligalAmountException {
        if (amount < 1 || amount > 11) {
            throw new IlligalAmountException("Amount is invalid!");
        }
        this.amount = amount;
    }

    public int getCostInCents() {
        int cost = 0;
        for (int i = 0; i < ingredients.size(); i++) {
            cost += ingredients.get(i).getPriceInCents();
        }
        return cost;
    }

    public String getName() {
        return name;
    }

    public boolean isCalzone() {
        return isCalzone;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return name + " : " + amount;
    }

    // Pizzas are the same if they have the same ingredients and type.
    // Although their names and amounts can be different(because the program can
    // create unique names).
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Pizza other = (Pizza) obj;
        return ingredients.equals(other.ingredients)
                && isCalzone == other.isCalzone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredients, isCalzone);
    }

}
