package com.olpi.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.olpi.exception.IlligalAmountException;

public class Order {
    private static int idCounter = 10001;

    private final int orderId;
    private final int clientId;
    private List<Pizza> pizzas;
    private LocalDate date;

    public Order(int clientId) {
        orderId = idCounter++;
        this.clientId = clientId;
        pizzas = new ArrayList<>();
        date = LocalDate.now();
    }

    public int getOrderId() {
        return orderId;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public int getClientId() {
        return clientId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void addPizza(Pizza pizza) throws IlligalAmountException {

        if (pizzas.contains(pizza)) {
            for (int i = 0; i < pizzas.size(); i++) {
                if (pizzas.get(i).equals(pizza)) {

                    if (pizzas.get(i).getAmount() + pizza.getAmount() > 10) {
                        throw new IlligalAmountException(
                                "You cannot add more than 10 same pizzas!");
                    } else {
                        pizzas.get(i).setAmount(
                                pizzas.get(i).getAmount() + pizza.getAmount());
                    }
                }
            }
        } else {
            setPizzaName(pizza);
            pizzas.add(pizza);
        }
    }

    public void changeAmountOfPizza(Pizza pizza, int newAmount)
            throws IlligalAmountException {

        if (newAmount > 10 || newAmount < 1) {
            throw new IllegalArgumentException(
                    "You cannot specify that amount of pizza!");
        }

        for (int i = 0; i < pizzas.size(); i++) {
            if (pizzas.get(i).equals(pizza)) {
                pizzas.get(i).setAmount(newAmount);
            }
        }
    }

    public void removePizza(Pizza pizza) {

        for (int i = 0; i < pizzas.size(); i++) {
            if (pizzas.get(i).equals(pizza)) {
                pizzas.remove(i);
            }
        }
    }

    public double getTotalInCents() {
        int total = 0;
        for (int i = 0; i < pizzas.size(); i++) {
            total += pizzas.get(i).getCostInCents() * pizzas.get(i).getAmount();
        }
        return (double) total / 100;
    }

    public void printPizzaAtribute() {
        for (Pizza pizza : pizzas) {
            System.out.println("[" + orderId + " : " + clientId + " : "
                    + pizza.toString() + "]");
        }
    }

    @Override
    public String toString() {

        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("********************************");
        joiner.add("OrderId: " + orderId);
        joiner.add("ClientId: " + clientId);

        for (Pizza pizza : pizzas) {
            joiner.add("Name: " + pizza.getName());
            joiner.add("--------------------------------");

            for (Ingredient ingred : pizza.getIngredients()) {
                joiner.add(String.format("%-25s %.2f €", ingred.getName(),
                        (double) ingred.getPriceInCents() / 100));
            }
            joiner.add("--------------------------------");
            joiner.add(String.format("%-25s %.2f €", "Sum: ",
                    (double) pizza.getCostInCents() / 100));
            joiner.add(
                    String.format("%-25s %d", "Amount: ", pizza.getAmount()));
            joiner.add("--------------------------------");
        }
        joiner.add(String.format("%-25s %.2f €", "Total: ", getTotalInCents()));
        joiner.add("********************************");
        return joiner.toString();
    }

    private void setPizzaName(Pizza pizza) {

        int amountSymbols = latinSymbolsCounter(pizza.getName());
        if (amountSymbols < 4 || amountSymbols > 20) {
            pizza.setName(clientId + "_" + (pizzas.size() + 1));
        }
    }

    private int latinSymbolsCounter(String text) {
        int count = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.codePointAt(i) > 64 && text.codePointAt(i) < 123) {
                count++;
            }
        }
        return count;
    }
}
