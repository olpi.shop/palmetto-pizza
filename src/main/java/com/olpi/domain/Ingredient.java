package com.olpi.domain;

public enum Ingredient {
    TOMATO_PASTE("Tomato Paste", 100), CHEESE("Cheese", 100),
    SALAMI("Salami", 150), BACON("Bacon", 120), GARLIC("Garlic", 30),
    CORN("Corn", 70), PEPPERONI("Pepperoni", 60), OLIVES("Olives", 50),
    PIZZA_BAZE("Pizza Base", 100),
    PIZZA_BASE_CALZONE("Pizza Base (Calzone)", 150);

    private String name;
    private int price;

    Ingredient(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPriceInCents() {
        return price;
    }

}
