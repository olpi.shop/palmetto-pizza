package com.olpi.exception;

@SuppressWarnings("serial")
public class IlligalAmountException extends Exception {

    public IlligalAmountException() {
        super();
    }

    public IlligalAmountException(String message) {
        super(message);
    }

    public IlligalAmountException(String message, Throwable cause) {
        super(message, cause);
    }

}
